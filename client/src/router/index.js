import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/camisetas",
    name: "Camisetas",
    component: () => import("../views/Camisetas.vue")
  },
  {
    path: "/calcas",
    name: "Calcas",
    component: () => import("../views/Calcas.vue")
  },
  {
    path: "/calcados",
    name: "Calcados",
    component: () => import("../views/Calcados.vue")
  },
  {
    path: "/contato",
    name: "Contato",
    // route level code-splitting
    // this generates a separate chunk (contato.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "contato" */ "../views/Contato.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
