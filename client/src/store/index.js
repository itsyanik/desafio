import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);
Vue.use(axios);

export default new Vuex.Store({
  state: {
    navigationLinks: [],
    products: [],
    filteredProducts: [],
    isFilterActive: false,
    shirts: [],
    pants: [],
    shoes: []
  },
  mutations: {
    async getNavigationLinks(state) {
      let links = await axios.get(
        "http://localhost:8888/api/V1/categories/list"
      );
      state.navigationLinks = [...state.navigationLinks, links.data.items];
    },
    async getAllProducts(state) {
      let allProducts = [];
      let shirts = await axios.get("http://localhost:8888/api/V1/categories/1");
      let pants = await axios.get("http://localhost:8888/api/V1/categories/2");
      let shoes = await axios.get("http://localhost:8888/api/V1/categories/3");

      allProducts = [...allProducts, { shirts: shirts.data.items }];
      allProducts = [...allProducts, { pants: pants.data.items }];
      allProducts = [...allProducts, { shoes: shoes.data.items }];

      // flatten array
      allProducts = allProducts.reduce((subarray, i) => subarray.concat(i), []);
      state.products = [...state.products, allProducts];
      state.products = state.products[0];
    },
    orderBy(state, payload) {
      const product = payload.product;
      if (payload.attr === "gender" || payload.attr === "color") {
        if (product === "products") {
          state[product][0].sort((a, b) => {
            if (a.filter[0][payload.attr] !== undefined) {
              return a.filter[0][payload.attr].localeCompare(
                b.filter[0][payload.attr]
              );
            }
            return;
          });
        } else {
          state[product].sort((a, b) => {
            if (a.filter[0][payload.attr] !== undefined) {
              return a.filter[0][payload.attr].localeCompare(
                b.filter[0][payload.attr]
              );
            }
            return;
          });
        }
      } else if (payload.attr === "price") {
        if (product === "products") {
          state[product][0].sort((a, b) => a.price > b.price);
        } else {
          state[product].sort((a, b) => a.price > b.price);
        }
      }
    },
    findByColor(state, payload) {
      const color = payload.color;
      state.filteredProducts = [];

      state.products.map(product => {
        const type =
          payload.productType === "all"
            ? Object.keys(product)
            : payload.productType;
        if (product[type] !== undefined) {
          product[type].filter(obj => {
            if (obj.filter[0].color !== undefined) {
              if (obj.filter[0].color === color) {
                state.filteredProducts = [...state.filteredProducts, obj];
              }
            }
          });
        }
      });
    },
    findByCategory(state, payload) {
      const category = payload.category;
      state.filteredProducts = [];

      if (category === "clothing") {
        state.filteredProducts = [
          ...state.filteredProducts,
          state.products[1]["pants"]
        ];
        state.filteredProducts = [
          ...state.filteredProducts,
          state.products[0]["shirts"]
        ];
        state.filteredProducts = state.filteredProducts.reduce(
          (subarray, i) => subarray.concat(i),
          []
        );
      } else {
        state.products.map(product => {
          if (product[category] !== undefined) {
            state.filteredProducts = [
              ...state.filteredProducts,
              ...product[category]
            ];
          }
        });
      }
    },
    findByType(state, payload) {
      const type = payload.type;
      state.filteredProducts = [];
      state.products.map(category => {
        category[Object.keys(category)].filter(product => {
          if (product.name.toLowerCase().indexOf(type) !== -1) {
            state.filteredProducts = [...state.filteredProducts, product];
          }
        });
      });
    },
    activateFilter(state) {
      state.isFilterActive = true;
    },
    deactivateFilter(state) {
      state.isFilterActive = false;
    }
  },
  actions: {
    getNavigationLinks({ commit }) {
      commit("getNavigationLinks");
    },
    getAllProducts({ commit }) {
      commit("getAllProducts");
    },
    orderBy({ commit }) {
      commit("orderBy", {
        product: String,
        attribute: String
      });
    },
    activateFilter({ commit }) {
      commit("activateFilter");
    },
    deactivateFilter({ commit }) {
      commit("deactivateFilter");
    },
    findByColor({ commit }) {
      commit("findByColor", {
        color: String,
        productType: String
      });
    },
    findByCategory({ commit }) {
      commit("findByCategory", {
        category: String
      });
    },
    findByType({ commit }) {
      commit("findByType", {
        type: String
      });
    }
  },
  modules: {},
  getters: {
    getShirts(state) {
      const shirts = state.products[0]["shirts"];
      return shirts;
    },
    getPants(state) {
      const pants = state.products[1]["pants"];
      return pants;
    },
    getShoes(state) {
      const shoes = state.products[2]["shoes"];
      return shoes;
    }
  }
});
