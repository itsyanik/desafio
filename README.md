# Desafio Frontend - WebJump

## Instalação das dependências
Na pasta api e na Pasta Client:
```
npm install
```

### Iniciando a aplicação
Na pasta api:
```
npm start
```

Na pasta client:
```
npm run serve
```
### Tecnologias utilizadas

*VueJS
*NodeJS
*Axios

### Soluções

Escolhi o [VueJS](https://vuejs.org/) como principal tecnologia para desenvolver a aplicação por considerá-lo um framework intuitivo, e de fácil aprendizado. Também gosto da maneira como é fácil fazer a comunicação entre um componente e outro através da *store*.

#### Filtros

Os filtros foram definitivamente a parte mais desafiadora desse projeto. Foi necessário muita busca e teste até conseguir fazê-los funcionar de maneira adequada. Nunca havia feito nada do tipo, e foi um aprendizado e tanto!

#### Responsividade

Como o Vue permite setilos de CSS em escopo (não deixando que eles interfiram com outros componentes), essa parte foi facilitada em sua maior parte. Porém não consegui alinhar os produtos conforme o *preview* dos catálogos.

#### Bugs

Fiz o meu melhor para eliminar todos os bugs antes de entregar o projeto, porém não consegui resolver todos antes de entregar. Gosto de ser transparente, e com os bugs não poderia ser diferente. 

* Ao recarregar a página dentro das rotas /calcados, /camisetas, /calcas a aplicação não consegue renderizar
* Ao clicar em algum tipo de filtro dentro de alguma rota todos os produtos serão filtrados

### Conclusão

Gostaria de agradecer pela oportunidade de participar no processo seletivo, e espero que os meus acertos possam falar mais alto que meus erros. Foi muito legal fazer esse projeto e aprender com ele, e espero poder aprender mais com vocês.